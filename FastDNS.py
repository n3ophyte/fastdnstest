__author__ = 'sruggier'
"""
FastDNS Akamai Zone Checker
-------------------------

 The MIT License

 Copyright 2016 @n3ophyte

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

-------------------------
"""
from optparse import OptionParser
from easyzone import easyzone
import dns.resolver
import re,socket,time

USAGE = """
-v for version
-d for debug
-e only error on resolution or not found
-s print only summary
-z load a zone file require 2 arguments: zone.txt zonename (if @ is in zone file please replace it with zonename)
-n nameserver of Akamai to compare with
-L read records from a file list instead of a zone DB

example 1 print all the output:
    python FastDNS.py -z example.txt example.com.

example 2 print only summary:
    python FastDNS.py -z -s example.txt example.com.

example 3 print all not found entry:
    python FastDNS.py -z -e example.txt example.com.

example 4 check FastDNS NS:
    python FastDNS.py -z -n -s example.txt example.com. 1-54.akam.net
    python FastDNS.py -z -n -s [Zone File] [Zone full Name] [NS record to compare results with]]
    
"""
#Command line options
VERSION = "v1 FastDNS Test"

parser = OptionParser(USAGE)

parser.add_option("-v",
                  action="store_true", dest="version",
                  default=False,
                  help="show version")
parser.add_option("-d",
                  action="store_true", dest="debug",
                  default=False,
                  help="show debug information")
parser.add_option("-t",
                  action="store_true", dest="testing",
                  default=False,
                  help="TTL test not implemented yet")
parser.add_option("-z",
                  action="store_true", dest="zone_file",
                  default=False,
                  help="import Zone File")
parser.add_option("-n",
                  action="store_true", dest="nameserver",
                  default=False,
                  help="FastDNS Nameserver where to test")
parser.add_option("-e",
                  action="store_true", dest="not_found",
                  default=False,
                  help="FastDNS not found")
parser.add_option("-s",
                  action="store_true", dest="only_summary",
                  default=False,
                  help="FastDNS print only summary")
parser.add_option("-L",
                  action="store_true", dest="file_list",
                  default=False,
                  help="read record name from a file list instead of Zone DB")
(options, args) = parser.parse_args()

if len(args) == 0:
    print "Type FastDNS.py -h for help"
    exit()

if options.version:
    print VERSION
    exit()

if options.nameserver and options.zone_file:
    if len(args) < 3:
        print "-z and -n option require zone file name, zone name and nameserver. Ex: -n -z example.txt example.com myfavoritedns.com"
        exit(0)
    else:
        zone_file = args[0]
        zone_name = args[1]
        nameserver = args[2]
        #TODO: verify that file exist and namesserver is correct!!
if options.zone_file:
    if len(args) < 2:
        print "-z option require zone file name  and zone name Ex: -n example.txt example.com"
        exit(0)
    else:
        zone_file = args[0]
        zone_name = args[1]

if options.not_found and options.only_summary:
    print "You cant have only summary (-s) with not found options (-e)"
    exit()

if not options.zone_file:
    print "-z option is required!"
    exit()


#TODO: ????!!!!??? very bad too many temp vars need class with o-o code
def akamaNStest(key,rType,ns,external_answer):
    # Avoid flooding FastDNS
    time.sleep(1)
    noAnswer = False
    akamai_resolver = dns.resolver.Resolver()
    akamai_resolver.nameservers = [socket.gethostbyname(ns)]
    if options.debug:
        print "Resolving: "+key
    try:
        akamai_answer = akamai_resolver.query(key,rType)
    except dns.resolver.NoAnswer:
        noAnswer = True
    if not "MX" in rType:
        if not noAnswer:
            akamai_matches = re.findall(r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s'+'(.*)', akamai_answer.response.to_text())
    #print  akamai_answer.response.to_text()
    #print "......"
        real_matches = re.findall(r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s'+'(.*)', external_answer)
    else:
        if not noAnswer:
            akamai_matches = re.findall(r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s\d+\s+'+'(.*)', akamai_answer.response.to_text())
        real_matches = re.findall(r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s\d+\s+'+'(.*)', external_answer)
    #print external_answer
    #print "....."
    if noAnswer:
        return key+"| Akamai Response: No Answer Real Response "+real_matches[0]
    for matches in akamai_matches:
        if not options.only_summary:
            print "key: "+key
        if matches in real_matches:
            if not options.only_summary:
                print "Real DNS:"
                print real_matches
                print "FastDNS Response"
                print akamai_matches
            akamai_resolver.nameservers = ["8.8.8.8"]
            return None
        else:
            return key+"| Akamai Response: "+matches+" Real Response "+real_matches[0]


z = easyzone.zone_from_file(zone_name, zone_file)

'''
VAR DECLARATION
'''
record_A = {}
record_CNAME = {}
record_MX = {}
record_TXT = {}
not_found_A = []
not_found_CNAME = []
not_found_MX = []
not_found_TXT = []
not_found_aka_A = []
not_found_aka_CNAME = []
not_found_aka_MX = []
not_found_aka_TXT = []


for record in z.names.items():
    if options.debug:
        print "Zone Record Name: "+record[0]
    if record[1].records('A'):
        if options.debug:
            print "Zone Record Value"
            print record[1].records('A').items
        record_A[record[0]] = record[1].records('A').items[0]
    if record[1].records('CNAME'):
        if options.debug:
            print "Zone Record Value"
            print record[1].records('CNAME').items
        record_CNAME[record[0]] = record[1].records('CNAME').items[0]
    if record[1].records('MX'):
        if options.debug:
            print "Zone Record Value"
            print record[1].records('MX').items
            print len(record[1].records('MX').items)
        #TODO: bad coding rework multiple MX detection
        if len(record[1].records('MX').items) < 1:
            record_MX[record[0]] = record[1].records('MX').items[0]
        else:
            record_MX[record[0]] = {}
            for mx_records in record[1].records('MX').items:
                record_MX[record[0]][mx_records] = 0
    if record[1].records('TXT'):
        if options.debug:
            print "Zone Record Value"
            print record[1].records('TXT').items
        record_TXT[record[0]] = record[1].records('TXT').items[0]

if not options.only_summary:
    print "--------------- Check in progress -----------"
    print "---------------------------------------------"

my_resolver = dns.resolver.Resolver()
my_resolver.nameservers = ['8.8.8.8']
if not options.only_summary:
    print "--------------- Checking A Records ----------"
for key, value in record_A.iteritems():
    try:
        answer = my_resolver.query(key,"A")
    except dns.resolver.NoAnswer:
        if not options.only_summary:
            print "Not Found! value from file "+key+" "+value
        answer = my_resolver.query(key)
        if not options.only_summary:
            print answer.response.to_text()
        continue
    #response_section = answer.response.to_text().split(";ANSWER")
    response_regex = r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s' + re.escape(value)
    if re.search(response_regex, answer.response.to_text(), re.IGNORECASE):
        if not options.not_found and not options.only_summary:
            print "Found A Record! value from file "+key+" "+value
    else:
        if not options.only_summary:
            print "Not Found! value from file "+key+" "+value
            print "Value from DNS:"
            print answer.response.to_text()
        not_found_A.append(key)
    if options.nameserver:
        akam_response = akamaNStest(key,"A",nameserver,answer.response.to_text())
        if akam_response:
            not_found_aka_A.append(akam_response)
    if not options.only_summary:
        print "---------------------------------------------"
'''

'''
if not options.only_summary:
    print "--------------- Checking CNAME Records ------"
for key, value in record_CNAME.iteritems():
    try:
        answer = my_resolver.query(key,"CNAME")
    except dns.resolver.NoAnswer:
        if not options.only_summary:
            print "Not Found! value from file "+key+" "+value
        answer = my_resolver.query(key)
        if not options.only_summary:
            print answer.response.to_text()
        not_found_CNAME.append(key)
        if options.nameserver:
            akam_response = akamaNStest(key,"CNAME",nameserver,answer.response.to_text())
            if akam_response:
                not_found_aka_CNAME.append(akam_response)
        continue
    #response_section = answer.response.to_text().split(";ANSWER")
    response_regex = r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s' + re.escape(value)
    if re.search(response_regex, answer.response.to_text(), re.IGNORECASE):
        if not options.not_found and not options.only_summary:
            print "Found CNAME Record! value from file "+key+" "+value
    else:
        if not options.only_summary:
            print "Not Found! value from file "+key+" "+value
            print "Value from DNS:"
            print answer.response.to_text()
        not_found_CNAME.append(key)
    if options.nameserver:
        akam_response = akamaNStest(key,"CNAME",nameserver,answer.response.to_text())
        if akam_response:
            not_found_aka_CNAME.append(akam_response)
    if not options.only_summary:
        print "---------------------------------------------"
'''

'''
if not options.only_summary:
    print "--------------- Checking MX Records ---------"
for key, values in record_MX.iteritems():

    try:
        answer = my_resolver.query(key,"MX")
    except dns.resolver.NoAnswer:
        if not options.only_summary:
            print "Not Found! value from file "+key+" "+value
        answer = my_resolver.query(key)
        if not options.only_summary:
            print answer.response.to_text()
        continue
    #response_section = answer.response.to_text().split(";ANSWER")
    for value in values:
        response_regex = r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s\d+\s+' + re.escape(value[1])
        if re.search(response_regex, answer.response.to_text(), re.IGNORECASE):
            if not options.not_found and not options.only_summary:
                print "Found MX Record! value from file "+key+" "+value[1]
        else:
            if not options.only_summary:
                print "Not Found! value from file "+key+" "+value[1]
                print "Value from DNS:"
                print answer.response.to_text()
            not_found_MX.append(key)
        if options.nameserver:
            akam_response = akamaNStest(key,"MX",nameserver,answer.response.to_text())
            if akam_response:
                not_found_aka_MX.append(akam_response)
        if not options.only_summary:
            print "---------------------------------------------"
'''

'''
if not options.only_summary:
    print "--------------- Checking TXT Records --------"
for key, value in record_TXT.iteritems():
    try:
        answer = my_resolver.query(key,"TXT")
    except dns.resolver.NoAnswer:
        if not options.only_summary:
            print "Not Found! value from file "+key+" "+value
        answer = my_resolver.query(key)
        if not options.only_summary:
            print answer.response.to_text()
        continue
    #response_section = answer.response.to_text().split(";ANSWER")
    response_regex = r'' + re.escape(key) + '\s\d+\s\w+\s\w+\s' + re.escape(value)
    if re.search(response_regex, answer.response.to_text(), re.IGNORECASE):
        if not options.not_found and not options.only_summary:
            print "Found TXT Record! value from file "+key+" "+value
    else:
        if not options.only_summary:
            print "Not Found! value from file "+key+" "+value
            print "Value from DNS:"
            print answer.response.to_text()
        not_found_TXT.append(key)
    if options.nameserver:
        akam_response = akamaNStest(key,"TXT",nameserver,answer.response.to_text())
        if akam_response:
            not_found_aka_TXT.append(akam_response)
    if not options.only_summary:
        print "---------------------------------------------"


print "---------------- Summary --------------------"
print "---------------- Record A -------------------"
print "Total A Record Analyzed: "+str(len(record_A))
print "Error on A Record: "+str(len(not_found_A))
print "List of A Record Error:"
for record in not_found_A:
    print "     "+record
if options.nameserver:
    print "Error Between FastDNS and Real DNS: "+str(len(not_found_aka_A))
    print "List of Error Between FastDNS and Real DNS:"
    for akam_record in not_found_aka_A:
        print "     "+akam_record
print "---------------------------------------------"
print "---------------- Record CNAME ---------------"
print "Total CNAME Record Analyzed: "+str(len(record_CNAME))
print "Error on CNAME Record: "+str(len(not_found_CNAME))
print "List of CNAME Record Error:"
for record in not_found_CNAME:
    print "     "+record
if options.nameserver:
    print "Error Between FastDNS and Real DNS: "+str(len(not_found_aka_CNAME))
    print "List of Error Between FastDNS and Real DNS:"
    for akam_record in not_found_aka_CNAME:
        print "     "+akam_record
print "---------------------------------------------"
print "---------------- Record MX ------------------"
print "Total MX Record Analyzed: "+str(len(record_MX))
print "Error on MX Record: "+str(len(not_found_MX))
print "List of MX Record Error:"
for record in not_found_MX:
    print "     "+record
if options.nameserver:
    print "Error Between FastDNS and Real DNS: "+str(len(not_found_aka_MX))
    print "List of Error Between FastDNS and Real DNS:"
    for akam_record in not_found_aka_MX:
        print "     "+akam_record
print "---------------------------------------------"
print "---------------- Record TXT -----------------"
print "Total TXT Record Analyzed: "+str(len(record_TXT))
print "Error on TXT Record: "+str(len(not_found_TXT))
print "List of TXT Record Error:"
for record in not_found_TXT:
    print "     "+record
if options.nameserver:
    print "Error Between FastDNS and Real DNS: "+str(len(not_found_aka_TXT))
    print "List of Error Between FastDNS and Real DNS:"
    for akam_record in not_found_aka_TXT:
        print "     "+akam_record